package br.com.alan.loja.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(name = "doc_prd_codigo", referencedColumnName = "prd_codigo", foreignKey = @ForeignKey(name = "fk_doc_produto"))
@Table(name = "tab_doce")
public class Doce extends Produto {

	@Column(name = "doc_quantidade_minima", nullable = false)
	private Integer quantidadeMinima;

	public Doce() {
		super();
	}

	public Doce(String nome, String descricao, Double valorUnitario, Integer quantidadeMinima) {
		super(nome, descricao, valorUnitario);
		this.quantidadeMinima = quantidadeMinima;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((quantidadeMinima == null) ? 0 : quantidadeMinima.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Doce other = (Doce) obj;
		if (quantidadeMinima == null) {
			if (other.quantidadeMinima != null)
				return false;
		} else if (!quantidadeMinima.equals(other.quantidadeMinima))
			return false;
		return true;
	}

	public Integer getQuantidadeMinima() {
		return quantidadeMinima;
	}

	public void setQuantidadeMinima(Integer quantidadeMinima) {
		this.quantidadeMinima = quantidadeMinima;
	}

}
