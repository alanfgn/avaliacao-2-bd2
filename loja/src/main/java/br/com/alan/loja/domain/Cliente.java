package br.com.alan.loja.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "tab_cliente")
public class Cliente {

	@Id
	@Column(name = "cli_cpf", columnDefinition = "char(11)")
	private String cpf;

	@Column(name = "cli_nome", length = 40, nullable = false)
	private String nome;
	
	@Embedded
	@Column(nullable = false)
	private Endereco endereco;
	
	@ElementCollection
	@CollectionTable(name = "tab_telefones_cliente", 
		joinColumns = @JoinColumn(name = "ftl_cli_cpf", columnDefinition = "char(11)",
		referencedColumnName="cli_cpf", foreignKey = @ForeignKey(name = "fk_ftl_cliente")))
	@Column(name = "ftl_telefone", columnDefinition = "char(12)")
	private List<String> telefones; 
	

	@ManyToMany
	@JoinTable(name = "tab_clientes_produtos", 
		joinColumns = @JoinColumn(name = "cpr_cli_cpf", 
		referencedColumnName = "cli_cpf", columnDefinition = "char(11)", 
			foreignKey = @ForeignKey(name = "fk_cpr_clientes")), 
		inverseJoinColumns = @JoinColumn(name = "cpr_prd_codigo", referencedColumnName = "prd_codigo", 
			foreignKey = @ForeignKey(name = "fk_cpr_produtosq")))
	private List<Produto> produtos;
	
	public Cliente() {
		super();
	}
	
	public Cliente(String cpf, String nome, Endereco endereco, List<String> telefones) {
		this(cpf, nome, endereco, telefones, new ArrayList<>());
	}
	
	public Cliente(String cpf, String nome, Endereco endereco, List<String> telefones, List<Produto> produtos) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.endereco = endereco;
		this.telefones = telefones;
		this.produtos = produtos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((produtos == null) ? 0 : produtos.hashCode());
		result = prime * result + ((telefones == null) ? 0 : telefones.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (produtos == null) {
			if (other.produtos != null)
				return false;
		} else if (!produtos.equals(other.produtos))
			return false;
		if (telefones == null) {
			if (other.telefones != null)
				return false;
		} else if (!telefones.equals(other.telefones))
			return false;
		return true;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}
}
