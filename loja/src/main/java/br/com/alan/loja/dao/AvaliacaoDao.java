package br.com.alan.loja.dao;

import javax.persistence.EntityManager;

import br.com.alan.loja.domain.Bolo;
import br.com.alan.loja.domain.Cliente;
import br.com.alan.loja.factory.JpaUtil;

public class AvaliacaoDao {

	private EntityManager em = JpaUtil.getEntityManager();

	// 7
	public void persistir2Bolos1Ciente(Bolo bolo1, Bolo bolo2, Cliente cliente) {
		persistirBolo(bolo1);
		persistirBolo(bolo2);
		persistirCliente(cliente);
	}

	private void persistirBolo(Bolo bolo) {
		try {
			em.getTransaction().begin();
			em.persist(bolo);
			em.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		}
	}

	private void persistirCliente(Cliente cliente) {
		try {
			em.getTransaction().begin();
			em.persist(cliente);
			em.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		}
	}
	//

	// 8
	public void deletarBolo(Bolo bolo) {
		try {
			em.getTransaction().begin();
			em.remove(bolo);
			em.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		}
	}
	//

	// 9
	public void atualizarNomeBolo(String nome, Bolo bolo) {
		bolo.setNome(nome);
		atualizarBolo(bolo);
	}

	private void atualizarBolo(Bolo bolo) {
		try {
			em.getTransaction().begin();
			em.merge(bolo);
			em.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		}
	}
	//
	
	// 10
	public void imprimirDadosClienteNome(String nome){
		em.createQuery("select distinct c from Cliente c join fetch c.produtos where c.nome = :pNomeCliente", Cliente.class)
			.setParameter("pNomeCliente", nome).getResultList()
			.forEach(c -> {
				System.out.println("Endere�o: " + c.getEndereco());
				c.getProdutos().forEach(p -> System.out.println(p));
			});
	}
	// 

}
