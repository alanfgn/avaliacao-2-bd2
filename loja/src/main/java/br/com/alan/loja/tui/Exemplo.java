package br.com.alan.loja.tui;

import java.util.Arrays;

import br.com.alan.loja.dao.AvaliacaoDao;
import br.com.alan.loja.domain.Bolo;
import br.com.alan.loja.domain.Cliente;
import br.com.alan.loja.domain.Endereco;
import br.com.alan.loja.domain.TemaBoloEnum;

public class Exemplo {

	public static void main(String[] args) {

		AvaliacaoDao ava = new AvaliacaoDao();
		
		Bolo boloMorango = new Bolo("Morango", "Tem Morango", 20d, 8, TemaBoloEnum.INFANTIL);
		Bolo boloChocolate = new Bolo("Chocolate", null, 25d, 5, TemaBoloEnum.ADULTO);
		Cliente clienteJoaquim = new Cliente("11111111111", "Joaquim", new Endereco("Rua dos Bobos", "0", "Lugar nenhum"),
				Arrays.asList("010000000001", "020000000001"), Arrays.asList(boloMorango, boloChocolate));
				
		ava.persistir2Bolos1Ciente(boloMorango, boloChocolate, clienteJoaquim); // 7
		ava.deletarBolo(boloMorango);											// 8
		ava.atualizarNomeBolo("Negresco", boloChocolate);						// 9
		ava.imprimirDadosClienteNome("Joaquim");								// 10
	}

}
