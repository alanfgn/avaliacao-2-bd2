package br.com.alan.loja.domain;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import br.com.alan.loja.converters.TemaBoloEnumConverter;

@Entity
@PrimaryKeyJoinColumn(name = "bol_prd_codigo", referencedColumnName = "prd_codigo", foreignKey = @ForeignKey(name = "fk_bol_produto"))
@Table(name = "tab_bolo")
public class Bolo extends Produto {

	@Column(name = "bol_quantidade_fatias", nullable = false)
	private Integer quantidadeFatias;

	@Convert(converter = TemaBoloEnumConverter.class)
	@Column(name = "bol_tema", columnDefinition = "char(3)", nullable = false)
	private TemaBoloEnum tema;

	public Bolo() {
		super();
	}

	public Bolo(String nome, String descricao, Double valorUnitario, Integer quantidadeFatias, TemaBoloEnum tema) {
		super(nome, descricao, valorUnitario);
		this.quantidadeFatias = quantidadeFatias;
		this.tema = tema;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((quantidadeFatias == null) ? 0 : quantidadeFatias.hashCode());
		result = prime * result + ((tema == null) ? 0 : tema.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bolo other = (Bolo) obj;
		if (quantidadeFatias == null) {
			if (other.quantidadeFatias != null)
				return false;
		} else if (!quantidadeFatias.equals(other.quantidadeFatias))
			return false;
		if (tema != other.tema)
			return false;
		return true;
	}

	public Integer getQuantidadeFatias() {
		return quantidadeFatias;
	}

	public void setQuantidadeFatias(Integer quantidadeFatias) {
		this.quantidadeFatias = quantidadeFatias;
	}

	public TemaBoloEnum getTema() {
		return tema;
	}

	public void setTema(TemaBoloEnum tema) {
		this.tema = tema;
	}

}
