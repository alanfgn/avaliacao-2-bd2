package br.com.alan.loja.factory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {

	private static final String UNIT = "loja";

	private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(UNIT);

	public static EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

}
