package br.com.alan.loja.converters;

import javax.persistence.AttributeConverter;

import br.com.alan.loja.domain.TemaBoloEnum;

public class TemaBoloEnumConverter implements AttributeConverter<TemaBoloEnum, String> {

	@Override
	public String convertToDatabaseColumn(TemaBoloEnum tema) {
		return tema.getSigla();
	}

	@Override
	public TemaBoloEnum convertToEntityAttribute(String tema) {
		return TemaBoloEnum.valueOfSigla(tema);
	}

}
