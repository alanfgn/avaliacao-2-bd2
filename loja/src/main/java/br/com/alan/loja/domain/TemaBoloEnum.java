package br.com.alan.loja.domain;

public enum TemaBoloEnum {
	INFANTIL("IFT"), ADOLESCENTE("ADL"), ADULTO("ADT");

	private String sigla;

	private TemaBoloEnum(String sigla) {
		this.sigla = sigla;
	}
	
	public static TemaBoloEnum valueOfSigla(String sigla){
		for (TemaBoloEnum tema : TemaBoloEnum.values()) {
			if(tema.getSigla().equalsIgnoreCase(sigla))
				return tema;
		}
		throw new IllegalArgumentException();
	}

	public String getSigla() {
		return sigla;
	}

}
